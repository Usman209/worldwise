// "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=0&longitude=0"

import { useEffect, useState } from "react";

import styles from "./Form.module.css";
import Button from "./Button";
import BackButton from "./BackButton";
import { useUrlPosition } from "../hooks/useUrlPosition";
import  Message  from "./Message";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useCities } from "../context/CititesContext";
import { useNavigate } from "react-router-dom";

export function convertToEmoji(countryCode) {
  const codePoints = countryCode
    .toUpperCase()
    .split("")
    .map((char) => 127397 + char.charCodeAt());
  return String.fromCodePoint(...codePoints);
}

function Form ()
{
  const [ cityName, setCityName ] = useState( "" );
  const [country, setCountry] = useState("");
  const [ date, setDate ] = useState( new Date() );
  const [ notes, setNotes ] = useState( "" );
  const [ lat, lng ] = useUrlPosition()
  const [ isLoadingGeoCoding, SetIsLoadingGeoCoding ] = useState( false )
  const [ emoji, setEmoji ] = useState( "" )
  const [ error, SetError ] = useState( '' )
  const { createCity, loading } = useCities()
  const navigate = useNavigate()

  console.log(country,isLoadingGeoCoding);
  
  const BASE_URL = "https://api.bigdatacloud.net/data/reverse-geocode-client";


  useEffect(
    function ()
    {
    async function fetchCityData ()
    {
    try
  {
      
      SetIsLoadingGeoCoding( true )
      SetError('')
    const res = await fetch( `${ BASE_URL }?latitude=${ lat }&longitude=${ lng }` )
      const data = await res.json()
      if(!data.countryCode) throw new Error ('that is not a city click somewhere else ')
      console.log('this point ',data);
      setCityName( data.city || data.locality | "" )
      setCountry( data.countryName )
      setEmoji(convertToEmoji(data.countryCode))
  } catch ( error )
  {
    SetError(error.message)
  }
  finally
  {
    SetIsLoadingGeoCoding( false )

  }
}
    fetchCityData();
    }, [ lat, lng ] )
  
  
  function handleSubmit ( e )
  {
    e.preventDefault()
    if(!cityName || ! date) return
    
    const newCity = {
      cityName,
      country,
      emoji,
      date,
      notes,
      position:{lat:lat,lng: lng}
    }

    createCity( newCity )
    navigate('/app')
  }

  if ( error ) return <Message message={ error } />
  if ( !lat && !lng ) return <Message message='start by clikcing on map'  />

  return (
    <form className={`${styles.form} ${loading ? styles.loading :""}`}onSubmit={handleSubmit}>
      <div className={styles.row}>
        <label htmlFor="cityName">City name</label>
        <input
          id="cityName"
          onChange={(e) => setCityName(e.target.value)}
          value={cityName}
        />
        <span className={styles.flag}>{emoji}</span>
      </div>

      <div className={styles.row}>
        <label htmlFor="date">When did you go to { cityName }?</label>
        
        <DatePicker onChange={ ( date ) => setDate( date ) }
          selected={ date }
          dateFormat='dd/MM/yyyy'/>
        {/* <input  */}
          {/* id="date" */}
          {/* onChange={(e) => setDate(e.target.value)} */}
          {/* // value={date} */}
        {/* /> */}
      </div>

      <div className={styles.row}>
        <label htmlFor="notes">Notes about your trip to {cityName}</label>
        <textarea
          id="notes"
          onChange={(e) => setNotes(e.target.value)}
          value={notes}
        />
      </div>

     <div className={ styles.buttons }>
        <Button type="primary">Add</Button>
       <BackButton/>
      </div>
    </form>
  );
}

export default Form;
