 import { useNavigate } from 'react-router-dom'
// import { useNavigate } from 'react-router-dom'
import styles from './Map.module.css'
import { useCities } from '../context/CititesContext'
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  useMap,
  useMapEvents,
} from "react-leaflet";
import { useEffect, useState } from 'react';
import { useGeolocation } from '../hooks/useGeolocation';
import { useUrlPosition  } from "../hooks/useUrlPosition";

import Button from "./Button";

export default function Map ()
{
  // const navigate = useNavigate()
  const {cities} = useCities()
  const [ mapPosition, setMapPosition ] = useState( [ 40, 0 ] );
  const [lat,lng] = useUrlPosition()
  const { isLoading: isLoadingPosition, position: geolocationPosition, getPosition } = useGeolocation();

  console.log( setMapPosition );
  
  console.log('data is ',  cities);

  // const [ searchParams, setSearchParams ] = useSearchParams();
  // console.log(setSearchParams);

  
  useEffect( function ()
  {
    console.log(' map =====',lat , lng);
    if(lat & lng) setMapPosition([lat,lng])
    
  },[lat,lng])  

 useEffect( function ()
  {
    if(geolocationPosition) setMapPosition([geolocationPosition.lat,geolocationPosition.lan])
    
  },[geolocationPosition])  


  
  return (
 <div className={styles.mapContainer}>
      {!geolocationPosition && (
        <Button type="position" onClick={getPosition}>
          {isLoadingPosition ? "Loading..." : "Use your position"}
        </Button>
      )}

      <MapContainer
        center={mapPosition}
        zoom={6}
        scrollWheelZoom={true}
        className={styles.map}
      >    
     <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"
        />
        {cities.map((city) => (
          <Marker
            position={[city.position.lat, city.position.lng]}
            key={city.id}
          >
            <Popup>
              <span>{city.emoji}</span> <span>{city.cityName}</span>
            </Popup>
          </Marker>
          
        ) ) }
        <ChangeCenter position={ [ lat || 40, lng || 0 ] } />
        <DetectClick/>
        </MapContainer>
        </div>
  )
}

function ChangeCenter ( { position } )
{
  const map = useMap()
  map.setView( position )
  
  return null
  
}

function DetectClick ()
{
  const navigate = useNavigate();
  useMapEvents( {

    click: (  e ) => navigate(`form?lat=${e.latlng.lat}&lng=${e.latlng.lng}`)
  
  })
  
}