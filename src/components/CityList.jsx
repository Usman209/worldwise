
import styles from './CityList.module.css'
import Cityitem from './Cityitem';
import Spinner from "./Spinner";
import Message from "./Message"
import { useCities } from '../context/CititesContext';

export default function CityList (  )
{
  const { cities, loading } = useCities()
  
  if ( loading ) return <Spinner />;
  if ( !cities.length ) return (<Message message="add you first city by clicking on map" />)

  return (
  
    <ul className={ styles.cityList }>{ cities.map( ( city ) => ( <Cityitem city={city } key={city.id} />))} </ul>
  )
}
