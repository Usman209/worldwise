import { createContext, useContext, useReducer } from "react";

const AuthContext = createContext()

const intialState = {
  
  user: null,
  isAuthenticated: false
  
}
function reducer(state,action) {
  
  switch (action.type) {
    case 'login':
      return {
        ...state, isAuthenticated:true, user:action.payload
      }
    case 'logout': 
       return {
        ...state, isAuthenticated:false, user:null
      }
        
    default: throw new Error('not match any case')
  }
}

const FAKE_USER = {
  name: 'user',
  email: 'user@yahoo.com',
  password:'qwerty'
}

function AuthProvider ( { children } )
{
  const [{user,isAuthenticated},dispatch] = useReducer(reducer, intialState)
  

  function login (  email, password  )
  {
   
    if ( email === FAKE_USER.email && password === FAKE_USER.password )
    {

      dispatch({type:'login', payload:FAKE_USER})
    }
    
  }
  function logout ()
  {
    dispatch({type:'logout'})
    
  }

  return <AuthContext.Provider value={
    {
      user,
      isAuthenticated,
      login,
      logout
    }
  }>
    {children}
  </AuthContext.Provider>
}
function useAuth ()
{
  const context = useContext( AuthContext )
  if ( context === undefined )
  {
    throw new Error('error in useAuth')
  }
  return context;
  
}

export {AuthProvider,useAuth}