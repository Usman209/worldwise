import { createContext, useContext, useEffect, useReducer  } from 'react';

const CitiesContext = createContext();

const intialState = {
	cities: [],
	loading: false,
	currentCity: {},
	error: ''
};

function reducer(state, action) {
	switch (action.type) {
		case 'loading':
			return {
				...state,
				loading: true
			};
		case 'cities/loaded':
			return {
				...state,
				loading: false,
				cities: action.payload
			};
		case 'city/loaded':
			return {
				...state,
				loading: false,
				currentCity: action.payload
			};
		case 'city/created':
			return {
				...state,
				loading: false,
				cities: [ ...state.cities, action.payload ] // async way
			};
		case 'city/deleted':
			return {
				...state,
				loading: false,
				cities: state.cities.filter((city) => city.id !== action.payload)
			};
		case 'rejected':
			return {
				...state,
				loading: false,
				error: action.payload
			};
		default:
			throw new Error('unknow action type');
	}
}

function CitiesProvider({ children }) {
	const [ { cities, loading, currentCity }, dispatch ] = useReducer(reducer, intialState);

	// const [cities,setCities]=useState([])

	// const [ loading, setLoading ] = useState( false )
	// const [ currentCity, setCurrentCity ] = useState( {} )

	const BASEURL = 'http://localhost:8000';
	useEffect(function() {
		async function fetchCites() {
			dispatch({ type: 'loading' });
			try {
				const res = await fetch(`${BASEURL}/cities`);
				const data = await res.json();
				// setCities( data )
				dispatch({ type: 'cities/loaded', payload: data });
			} catch (error) {
				dispatch({
					type: 'rejected',
					payload: 'there was error loading data.... '
				});
			}
		}
		fetchCites();
	}, []);

  async function getCity ( id )
  {
    if(Number(id)===currentCity.id) return

		try {
			dispatch({ type: 'loading' });
			const res = await fetch(`${BASEURL}/cities/${id}`);
			const data = await res.json();
			// setCurrentCity( data )
			dispatch({ type: 'city/loaded', payload: data });
		} catch (error) {
			dispatch({
				type: 'rejected',
				payload: 'there was error loading city data.... '
			});
		}
	}
	async function deleteCity(id) {
		try {
			dispatch({ type: 'loading' });
			await fetch(`${BASEURL}/cities/${id}`, {
				method: 'DELETE'
			});
			// setCities( (cities)=>cities.filter((city)=>city.id !==id) )
			dispatch({ type: 'city/deleted', payload: id });
		} catch (error) {
			dispatch({
				type: 'rejected',
				payload: 'there was error loading city data.... '
			});
		}
	}

	async function createCity(newCity) {
		try {
			dispatch({ type: 'loading' });
			const res = await fetch(`${BASEURL}/cities`, {
				method: 'POST',
				body: JSON.stringify(newCity),
				headers: {
					'Content-Type': 'application/json'
				}
			});
			const data = await res.json();
			dispatch({ type: 'city/created', payload: data });
			// setCities((cities)=>[...cities,data])
			console.log(data);
		} catch (error) {
			dispatch({
				type: 'rejected',
				payload: 'there was error loading city data.... '
			});
		}
	}

	return (
		<CitiesContext.Provider
			value={{
				cities,
				loading,
				currentCity,
				getCity,
				createCity,
				deleteCity
			}}
		>
			{children}
		</CitiesContext.Provider>
	);
}

function useCities() {
	const context = useContext(CitiesContext);
	if (context === undefined) throw new Error('CitiesContext was used outside the CitiesProvider');
	return context;
}

export { CitiesProvider, useCities };
